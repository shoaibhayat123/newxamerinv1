﻿using System;
using System.Collections.Generic;
using System.Linq;

using Foundation;
using UIKit;
using Plugin.FirebasePushNotification;
using System.Threading.Tasks;

namespace App14.iOS
{
    // The UIApplicationDelegate for the application. This class is responsible for launching the 
    // User Interface of the application, as well as listening (and optionally responding) to 
    // application events from iOS.
    [Register("AppDelegate")]
    public partial class AppDelegate : global::Xamarin.Forms.Platform.iOS.FormsApplicationDelegate
    {
        //
        // This method is invoked when the application has loaded and is ready to run. In this 
        // method you should instantiate the window, load the UI into it and then make the window
        // visible.
        //
        // You have 17 seconds to return from this method, or iOS will terminate your application.
        //
        public override bool FinishedLaunching(UIApplication app, NSDictionary options)
        {
            global::Xamarin.Forms.Forms.Init();
            LoadApplication(new App());
            //If debug you should reset the token each time.
#if DEBUG
            FirebasePushNotificationManager.Initialize(options, true);
#else
              FirebasePushNotificationManager.Initialize(options,false);
#endif
            CrossFirebasePushNotification.Current.OnTokenRefresh += async (s, p) =>
            {
                System.Diagnostics.Debug.WriteLine($"DEVICE TOKEN : {p.Token}");
                //GetMainPage("DEVICE TOKEN : " + p.Token);
                App.device_token = p.Token;
                Task.Run(async () =>
                {
                    await App.RegTokenOnServer(p.Token);
                }).Wait();
            };

            //Handle notification when app is closed here
            CrossFirebasePushNotification.Current.OnNotificationReceived += (s, p) =>
            {
                foreach (var data in p.Data)
                {
                    System.Diagnostics.Debug.WriteLine($"{data.Key} : {data.Value}");
                    if (data.Key == "recordid")
                    {
                        App.ticketID = data.Value.ToString();
                    }
                }
            };
            return base.FinishedLaunching(app, options);
        }
    }
}
