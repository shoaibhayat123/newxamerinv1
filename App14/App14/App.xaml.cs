﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Xamarin.Forms;
using SQLite;
using PCLStorage;
using App14.Models;
using Plugin.FirebasePushNotification;
using System.Net.Http;
using System.Threading.Tasks;
using Android.Util;

//using Quobject.SocketIoClientDotNet.Client;

namespace App14
{
    public partial class App : Application
    {
        //private static Page page;

        static SqlHelper database;        
        public static MasterDetailPage MasterDetail { get; set; }
        public static string session_string = "";
        public static string api_url = "";
        public static string url_username = "";
        public static string logged_user_name = "";
        public static string user_email = "";
        public static string user_id="";
        public static string school_name="";
        public static string tenant_id = "";
        public static int length;
        public static int width;
        public static string ticketID = "";

        public static string device_token = "";
        public static string group_id = "";
        public static string group_name = "";
        public static string dataTimeString = "";
        public static string accountEmail = "";
        public static string previousToken = "";

        public static EventsList selectedEvent = null;

        public async static void NavigateMasterDetail(Page page)
        {
            try
            {
                App.MasterDetail.IsPresented = false;
                await App.MasterDetail.Detail.Navigation.PushAsync(page);
            }
            catch
            { }
        }

        public async static void PopMasterDetail(int BackCount)
        {
            try
            {
                for (var counter = 0; counter < BackCount; counter++)
                {
                    //int count = Convert.ToInt32(App.MasterDetail.Detail.Navigation.NavigationStack.Count.ToString());
                    //await App.MasterDetail.Detail.Navigation.RemovePage(App.MasterDetail.Detail.Navigation.NavigationStack[count - 1]);

                    await App.MasterDetail.Detail.Navigation.PopAsync();
                }
                
            }
            catch
            { }
        }

        public static Page GetMainPage(string msg)
        {
            return new POPUPMainPage(msg);
        }

        public async void getSchoolName()
        {
            //List<RegEntity> reg = await App.Database.GetAllItem();
            //if (reg.Count() > 0)
            //{
            //    foreach (var item in reg)
            //    {
            //        GetMainPage("Reg Users " + item.school_name + " : " + item.Url + " : " + item.ID);
            //    }
            //}

            RegEntity userDetail = await App.Database.GetItemFirst();
            if (userDetail != null)
            {
                school_name = userDetail.school_name;
                api_url = userDetail.CompleteUrl;
            }
        }

        public async static Task getAllRememberCredentials()
        {
            List<RememberMeCredentials> userCredentialslst = await App.Database.GetAllRemeber();
            if (userCredentialslst.Count() > 0)
            {
                foreach (var item in userCredentialslst)
                {
                    //GetMainPage("Users " + item.url_name + " : " + item.user_id + " : " + item.ID + " : " + item.password +
                    //    " : " + item.user_name + " : " + item.password + " : " + item.user_tenant_id + " : " + item.session_string);
                    App.session_string = item.session_string;
                }
            }

            //RememberMeCredentials userCredentialslst = await App.Database.GetSaveRemeber();
            //App.session_string = userCredentialslst.session_string;
        }

        public async static Task<int> getAllRegisCredentials()
        {
            List<RegEntity> userD = await App.Database.GetAllItem();
            int count = 0;
            if (userD.Count > 0)
            {
                foreach (var item in userD)
                {
                    count++;
                    url_username = item.Url;
                    school_name = item.Url;
                    api_url = item.CompleteUrl;
                    System.Diagnostics.Debug.WriteLine($"IS Platform  itm " + item.Url + item.ID);
                }
            }
            return count;
        }

        public async static Task getAllPushNotifications()
        {
            try
            {
                List<PushNotificationData> pushNotifylst = await App.Database.GetAllPushNotification();
                if (pushNotifylst.Count() > 0)
                {
                    foreach (var item in pushNotifylst)
                    {
                        //GetMainPage("Notifications " + item.recordid + " : " + item.user_id + " : " + item.ID + " : " + item.body +
                        //    " : " + item.message + " : " + item.title + " : " + item.click_action + " : " + item.created);
                    }
                }
            }
            catch(Exception x) { GetMainPage($"IS Platform  1 {x}"); }
        }

        public async static Task deleteAllRememberCredentials()
        {
            List<RememberMeCredentials> userCredentialslst = await App.Database.GetAllRemeber();
            if (userCredentialslst.Count() > 0)
            {
                foreach (var item in userCredentialslst)
                {
                    //GetMainPage("Users " + item.url_name + " : " + item.user_id + " : " + item.ID + " : " + item.password +
                    //    " : " + item.user_name + " : " + item.password + " : " + item.user_tenant_id);

                    await App.Database.DeleteSaveUser(item);
                }
            }
        }

        public async static Task deleteAllRegisCredentials()
        {
            List<RegEntity> userD = await App.Database.GetAllItem();
            if (userD.Count > 0)
            {
                foreach (var item in userD)
                {
                    System.Diagnostics.Debug.WriteLine($"IS Platform  itm " + item.Url + item.ID);
                    await App.Database.DeleteItem(item);
                }
            }
        }

        public App()
        {
            InitializeComponent();
            try
            {
                //getSchoolName();                
                Task.Run(async () =>
                {
                    await RegNotification();
                    await getAllRememberCredentials();
                    await getAllPushNotifications();
                    System.Diagnostics.Debug.WriteLine($"IS Platform  2");
                    if (App.session_string != "")
                    {
                        System.Diagnostics.Debug.WriteLine($"IS Platform  user");
                        await getAllRegisCredentials();
                        //RegEntity userDetail = await App.Database.GetItemFirst();
                        //System.Diagnostics.Debug.WriteLine($"IS Platform  userDetail " + userDetail.Url);
                        //if (userDetail != null)
                        //{
                        //    //school_name = userDetail.school_name;     commit by shoaib
                        //    url_username = userDetail.Url;
                        //    school_name = userDetail.Url;
                        //    api_url = userDetail.CompleteUrl;
                        //}
                    }
                }).Wait();


                if (school_name != "" && school_name != null)
                {
                    //MainPage = new NavigationPage(new Events());
                    MainPage = new NavigationPage(new DefaultORNewSchool());
                    //NavigateMasterDetail(new MainPage());
                }
                else
                {
                    //MainPage = new NavigationPage(new Events());
                    MainPage = new NavigationPage(new SchoolLog());
                }
            }
            catch(Exception x) {
                System.Diagnostics.Debug.WriteLine($"IS Platform  user x " + x);
            }
        }



        // Shoaib coding
        public static async Task RegTokenOnServer(string token)
        {
            RememberMeCredentials userCredentials = await App.Database.GetRemeber();
            if (userCredentials != null)
            {
                //GetMainPage("User with Tenant " + userCredentials.url_name);
                user_id = userCredentials.user_id;
                tenant_id = userCredentials.user_tenant_id;
                //await page.DisplayAlert("Alert", $"User with Tenant {user_id + tenant_id}", "ok");
                // GetMainPage("User with Tenant " + user_id);
                var os = "";
                if (Device.RuntimePlatform == Device.iOS)
                {
                    os = "ios";
                }
                else if (Device.RuntimePlatform == Device.Android)
                {
                    os = "android";
                }
                System.Diagnostics.Debug.WriteLine($"IS Platform  : {os}");
                // await page.DisplayAlert("Alert", $"IS Platform  : {os}", "ok");
                //GetMainPage("OS " + os);
                var notify = new Dictionary<string, string>();
                notify.Add("operation", "insert_device_token");
                notify.Add("user_id", user_id);
                notify.Add("parent_id", tenant_id);
                notify.Add("device_token", token);
                notify.Add("device_type", os);
                var client = new HttpClient();
                client.BaseAddress = new Uri(App.api_url);
                var notify_content = new FormUrlEncodedContent(notify);
                HttpResponseMessage notify_response = await client.PostAsync("/itcrm/webservices/", notify_content);
                var notify_result = await notify_response.Content.ReadAsStringAsync();
                System.Diagnostics.Debug.WriteLine($"Result : {notify_result}");
                //await page.DisplayAlert("Alert", $"Result : {notify_result}", "ok");
                //GetMainPage("Result " + notify_result);
            }
        }


        public static async Task RegNotification()
        {
            try
            {
                CrossFirebasePushNotification.Current.OnTokenRefresh += async (s, p) =>
                {
                    System.Diagnostics.Debug.WriteLine($"DEVICE TOKEN : {p.Token}");
                    //GetMainPage("DEVICE TOKEN : " + p.Token);
                    device_token = p.Token;
                    Task.Run(async () =>
                    {
                        await RegTokenOnServer(p.Token);
                    }).Wait();
                };

                CrossFirebasePushNotification.Current.OnNotificationReceived += async (s, p) =>
                {
                    string TAG = "Cloudschool";
                    PushNotificationData saveNotifyInfo = new PushNotificationData();
                    foreach (var data in p.Data)
                    {
                        Log.Info(TAG, $"This Data {data.Key} : {data.Value}");
                        System.Diagnostics.Debug.WriteLine($"{data.Key} : {data.Value}");
                        if (data.Key == "recordid")
                        {
                            App.ticketID = data.Value.ToString();
                            saveNotifyInfo.recordid = data.Value.ToString();
                            Log.Debug(TAG, $"This is a debug message : {App.device_token}");
                        }
                        if (data.Key == "created")
                        {
                            saveNotifyInfo.created = data.Value.ToString();
                        }
                        if (data.Key == "user_id")
                        {
                            saveNotifyInfo.user_id = data.Value.ToString();
                        }
                        if (data.Key == "body")
                        {
                            saveNotifyInfo.body = data.Value.ToString();
                        }
                        if (data.Key == "title")
                        {
                            saveNotifyInfo.title = data.Value.ToString();
                        }
                        if (data.Key == "click_action")
                        {
                            saveNotifyInfo.click_action = data.Value.ToString();
                        }
                        if (data.Key == "message")
                        {
                            saveNotifyInfo.message = data.Value.ToString();
                        }
                        if (data.Key == "content_available")
                        {
                            saveNotifyInfo.content_available = data.Value.ToString();
                        }
                        if (data.Key == "sound")
                        {
                            saveNotifyInfo.sound = data.Value.ToString();
                        }
                        if (data.Key == "screen")
                        {
                            saveNotifyInfo.screen = data.Value.ToString();
                        }

                    }
                    var rec = await App.Database.SavePushNotification(saveNotifyInfo);
                    Log.Info(TAG, $"This Notification Data ID : {rec}");
                };

                CrossFirebasePushNotification.Current.OnNotificationOpened += (s, p) =>
                {
                   // //string ticketID = "";
                   // foreach (var data in p.Data)
                   // {
                   //     System.Diagnostics.Debug.WriteLine($"{data.Key} : {data.Value}");
                   //     if (data.Key == "recordid")
                   //     {
                   //         ticketID = data.Value.ToString();
                   //     }
                   // }

                   // if (!string.IsNullOrEmpty(p.Identifier))
                   // {
                   //     System.Diagnostics.Debug.WriteLine($"ActionId: {p.Identifier}");
                   // }
                };
            }
            catch
            {
                //Application.Current.Properties["token "] = "";
                //await Application.Current.SavePropertiesAsync();
                //RegNotification();
            }
        }


        public static SqlHelper Database
        {
            get
            {
                if (database == null)
                {
                    database = new SqlHelper();
                }
                return database;
            }
        }

        protected override void OnStart()
        {
              //RegNotification();
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            //RegNotification();
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            //RegNotification();
            // Handle when your app resumes
        }
    }
}
