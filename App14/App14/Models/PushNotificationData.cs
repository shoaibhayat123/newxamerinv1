﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App14.Models
{
    public class PushNotificationData
    {
        public PushNotificationData()
        {
        }
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string created { get; set; }
        public string recordid { get; set; }
        public string user_id { get; set; }
        public string body { get; set; }
        public string title { get; set; }
        public string click_action { get; set; }
        public string message { get; set; }
        public string content_available { get; set; }
        public string sound { get; set; }
        public string screen { get; set; }        
    }
}
