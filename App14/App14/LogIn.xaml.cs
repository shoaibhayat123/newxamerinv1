﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SQLite;
using PCLStorage;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using App14.Models;
using Plugin.FirebasePushNotification;

namespace App14
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LogIn : ContentPage
    {
        ///RememberMeCredentials remObj = new RememberMeCredentials();
        ComClass comfun = new ComClass();
        public static string URL_NAME;
        public LogIn(string url_name)
        {
            InitializeComponent();
            URL_NAME = url_name;
            
            //DisplayAlert("URL_NAME", URL_NAME + " USERNAME duplicate: " + App.url_username + " " + remObj.user_name + " " + remObj.user_name, "ok");
            try
            {
                NavigationPage.SetHasBackButton(this, false);
            }
            catch { }
        }

        public async void LoadCred()
        {
            try
            {
               // RememberMeCredentials saveRemInfo2 = await App.Database.GetSaveRemeber();
                RememberMeCredentials remObj = new RememberMeCredentials();
                //remObj = await App.Database.GetSaveRemeber();   commit by shoaib
                remObj = await App.Database.GetRemeber();
                if (remObj != null)
                {
                    //await DisplayAlert("Detail", "record exist", "OK");
                    TxtUserName.Text = remObj.user_name;
                    TxtPassword.Text = remObj.password;
                }
                else
                {
                   // await DisplayAlert("Detail", "no record exist", "OK");
                }
                
            }
            catch(Exception e)
            {
                await DisplayAlert("Error", e.Message, "ok");
            }
        }

        protected override void OnAppearing()
        {
            base.OnAppearing();
            LoadCred();
        }
        

        private async void btnLoginLbl_Clicked(object sender, EventArgs e)
        {
            if (comfun.isConnected())
            {
                btnLoginLbl.IsEnabled = false;
                Login();
            }
            else
            {
                await DisplayAlert("Internet Disabled", "You are not connected to internet", "ok");
                aiLogin.IsVisible = false;
                aiLogin.IsRunning = false;
                btnLoginLbl.IsEnabled = true;
            }
        }

        private void Login()
        {
            try
            {
                if (string.IsNullOrEmpty(TxtUserName.Text) && string.IsNullOrEmpty(TxtPassword.Text))
                {
                    btnLoginLbl.IsEnabled = true;
                    DisplayAlert("Empty Fields", "Email and Password are required", "Ok");
                    TxtUserName.Focus();
                }
                else if (string.IsNullOrEmpty(TxtUserName.Text))
                {
                    btnLoginLbl.IsEnabled = true;
                    DisplayAlert("Username", "Username is not given", "Ok");
                    TxtUserName.Focus();
                }
                else if (string.IsNullOrEmpty(TxtPassword.Text))
                {
                    btnLoginLbl.IsEnabled = true;
                    DisplayAlert("Password", "Password is not given", "Ok");
                    TxtPassword.Focus();
                }
                else
                {
                    try
                    {
                        string userName = TxtUserName.Text;
                        string password = TxtPassword.Text;
                        PostLogin();
                    }
                    catch (Exception e)
                    {
                        aiLogin.IsVisible = false;
                        aiLogin.IsRunning = false;
                        btnLoginLbl.IsEnabled = true;
                        DisplayAlert("Error!", "Login Fail " + e.Message, "Ok");
                    }
                }
            }
            catch
            {
                aiLogin.IsVisible = false;
                aiLogin.IsRunning = false;
                btnLoginLbl.IsEnabled = true;
            }
        }

        public async void PostLogin()
        {
            if (comfun.isConnected())
            {
                aiLogin.IsVisible = true;
                aiLogin.IsRunning = true;
                try
                {
                    string userName = TxtUserName.Text;
                    string password = TxtPassword.Text;
                    // await DisplayAlert("App.api_url", App.api_url + " " + userName + " " + password, "ok");
                    var client = new HttpClient();
                    client.BaseAddress = new Uri(App.api_url);
                    var values = new Dictionary<string, string>();
                    values.Add("operation", "loginI");
                    values.Add("url", URL_NAME);
                    values.Add("username", userName);
                    values.Add("password", password);
                    var content = new FormUrlEncodedContent(values);
                    HttpResponseMessage response = await client.PostAsync("/itcrm/webservices/", content);
                    var result = await response.Content.ReadAsStringAsync();
                    //await DisplayAlert("result!", result, "OK");
                    System.Diagnostics.Debug.WriteLine($"Result : {result}");
                    LoginData lgn = JsonConvert.DeserializeObject<LoginData>(result);                    
                    LoginDataResult result2 = new LoginDataResult();
                    result2 = lgn.result[0];                    
                    if (lgn.status == true)
                    {
                        //var token = "";
                        //CrossFirebasePushNotification.Current.OnTokenRefresh += (s, p) =>
                        //{
                        //    System.Diagnostics.Debug.WriteLine($"DEVICE TOKEN : {p.Token}");
                        //    token = p.Token;
                        //};
                        var session = result2.session_string;
                        App.session_string = Convert.ToString(session);
                        var email = result2.user_email;
                        App.user_email = Convert.ToString(email);
                        App.logged_user_name = Convert.ToString(result2.user_full_name);
                        App.user_id = Convert.ToString(result2.user_id);
                        App.tenant_id = result2.user_tenant_id;
                        System.Diagnostics.Debug.WriteLine($"DEVICE TOKEN LOGIN : {App.device_token}");
                        System.Diagnostics.Debug.WriteLine($"tenant_id  : {App.tenant_id}");
                        System.Diagnostics.Debug.WriteLine($"user_id  : {App.user_id}");
                        var os = "";
                        if (Device.RuntimePlatform == Device.iOS)
                        {
                            os = "ios";
                        }
                        else if (Device.RuntimePlatform == Device.Android)
                        {
                            os = "android";
                        }
                        System.Diagnostics.Debug.WriteLine($"IS Platform  : {os}");
                        var notify = new Dictionary<string, string>();
                        notify.Add("operation", "insert_device_token");
                        notify.Add("user_id", App.user_id);
                        notify.Add("parent_id", result2.user_tenant_id);
                        notify.Add("device_token", App.device_token);
                        notify.Add("device_type", os);
                        var notify_content = new FormUrlEncodedContent(notify);
                        HttpResponseMessage notify_response = await client.PostAsync("/itcrm/webservices/", notify_content);
                        var notify_result = await notify_response.Content.ReadAsStringAsync();
                        Application.Current.Properties["token "] = "";
                        await Application.Current.SavePropertiesAsync();
                        //await DisplayAlert("result!", result, "OK");
                        System.Diagnostics.Debug.WriteLine($"Result : {notify_result}");


                        //save in database
                        if (rememberMe.Checked == true)
                        {
                            try
                            {
                                //List<RememberMeCredentials> userCredentialslst = await App.Database.GetAllRemeber();
                                //foreach (var item in userCredentialslst)
                                //{
                                //    await App.Database.DeleteSaveUser(item);
                                //}
                                Task.Run(async () =>
                                {
                                    await App.deleteAllRememberCredentials();
                                }).Wait();
                                
                                RememberMeCredentials saveRemInfo = new RememberMeCredentials();
                                saveRemInfo.url_name = URL_NAME;
                                saveRemInfo.user_name = TxtUserName.Text;
                                saveRemInfo.password = TxtPassword.Text;
                                saveRemInfo.user_id = App.user_id;
                                saveRemInfo.user_tenant_id = result2.user_tenant_id;
                                saveRemInfo.session_string = App.session_string;
                                var rec = await App.Database.SaveRememberMeInfo(saveRemInfo);
                                //await DisplayAlert("rec", "record " + rec.ToString() + " Inserted.", "ok");
                            }
                            catch { }
                        }///////////////////////////////////////////////////////////////////////////
                        else
                        {
                            try
                            {
                                RememberMeCredentials a = new RememberMeCredentials();
                                //a.ID = 1;
                                var rec = await App.Database.DeleteSaveUser(a);
                                // await DisplayAlert("rec", rec.ToString() + " deleted.", "ok");
                            }
                            catch (Exception e)
                            {
                                // await DisplayAlert("error", "during " + e.Message, "ok");
                            }
                        }

                        // App.tenant_id = Convert.ToString(result2.te)
                        // await DisplayAlert("app save !", "id : " + App.user_id + " UN : " + App.logged_user_name + " App.user_email : " + App.user_email + " App.session_string " + App.session_string, "OK");
                        aiLogin.IsVisible = false;
                        aiLogin.IsRunning = false;
                        btnLoginLbl.IsEnabled = true;
                        await Navigation.PushAsync(new MainPage());
                    }
                    else
                    {
                        aiLogin.IsVisible = false;
                        aiLogin.IsRunning = false;
                        btnLoginLbl.IsEnabled = true;
                        await DisplayAlert("Error", result2.message + "...", "ok");
                    }
                }
                catch (Exception e)
                {
                    btnLoginLbl.IsEnabled = true;
                    aiLogin.IsVisible = false;
                    aiLogin.IsRunning = false;
                    await DisplayAlert("Error!", "Due to => " + e.Message, "OK");
                }
            }
            else
            {
                await DisplayAlert("Connection", "Internet Connection Disabled", "Ok");
            }
        }

    }

}