﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App14
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Chat : ContentPage
    {
        public Chat()
        {
            InitializeComponent();
        }

        protected override bool OnBackButtonPressed()
        {
            App.PopMasterDetail(1);
            base.OnBackButtonPressed();
            return true;
        }
    }
}