﻿using App14.Models;
using Plugin.LocalNotifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App14
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class addSchedule : ContentPage
    {
        location loc = new location();
        public static double btnLocationX;
        public static double btnLocationY;

        public addSchedule()
        {
            try
            {
                InitializeComponent();
                
                AbsoluteLayout.SetLayoutBounds(absMain, new Rectangle(0, 0, location.screenX, location.screenY - 25));
                reminderPicker.Items.Add("10 mins before, Notification");
                reminderPicker.Items.Add("20 mins before, Notification");
                reminderPicker.Items.Add("30 mins before, Notification");
                if (App.selectedEvent != null)
                {
                    DisplayAlert("Current Alert ", App.selectedEvent.ID +  App.selectedEvent.notificationTime + App.selectedEvent.endTime + App.selectedEvent.startTime + App.selectedEvent.startDate + App.selectedEvent.endDate, "OK");
                    reminderTitle.Text = App.selectedEvent.title;
                    startDatepick.Date = DateTime.ParseExact(App.selectedEvent.startDate, "d-M-yyyy", new System.Globalization.CultureInfo("fi-FI"));
                    //startDatepick.Date = Convert.ToDateTime(App.selectedEvent.startDate);
                    Datepick.Date = DateTime.ParseExact(App.selectedEvent.endDate, "d-M-yyyy", new System.Globalization.CultureInfo("fi-FI"));
                    //Datepick.Date = Convert.ToDateTime(App.selectedEvent.endDate);
                    startTimepick.Time = TimeSpan.Parse(App.selectedEvent.startTime);
                    EndTimepick.Time = TimeSpan.Parse(App.selectedEvent.endTime);
                    emailPicker.SelectedItem = App.selectedEvent.email;
                    emailPicker.Title = App.selectedEvent.email;
                    reminderPicker.SelectedItem = App.selectedEvent.notificationTime;
                    lblSDatePicked.Text = getSpecificDateFormat(startDatepick.Date);
                    lblDatePicked.Text = getSpecificDateFormat(Datepick.Date);
                    lblEndTimePicked.Text = EndTimepick.Time.ToString();
                    lblStartTimePicked.Text = startTimepick.Time.ToString();

                    //lblEndTimePicked.Text = getSpecificTimeFormat2(Convert.ToDateTime(EndTimepick.Time));
                    //lblStartTimePicked.Text = getSpecificTimeFormat2(Convert.ToDateTime(startTimepick.Time));
                }
                else
                {
                    emailPicker.Title = App.accountEmail == "" ? "" : App.accountEmail;
                    DateTime now = DateTime.Now.AddMinutes(14).ToLocalTime();
                    lblDatePicked.Text = getSpecificDateFormat(now);
                    string endtime = getSpecificTimeFormat2(addHourInCurrentTime(now, 1));
                    lblEndTimePicked.Text = endtime;

                    lblSDatePicked.Text = getSpecificDateFormat(now);
                  //  string time = getSpecificTimeFormat2(addHourInCurrentTime(now, 0));
                    string time = getSpecificTimeFormat2(addHourInCurrentTime(now, 0));
                    lblStartTimePicked.Text = time;
                    startTimepick.Time = TimeSpan.Parse(time);
                    EndTimepick.Time = TimeSpan.Parse(endtime);
                }
                /*btnLocationX = location.btnMenuLocationX;
                btnLocationY = location.btnMenuLocationY;                
                AbsoluteLayout.SetLayoutBounds(absMain, new Rectangle(0, 0, location.screenX, location.screenY - 25));
                AbsoluteLayout.SetLayoutBounds(backlayout, new Rectangle(0, 0, location.screenX, location.screenY));
                AbsoluteLayout.SetLayoutBounds(btnlayout, new Rectangle(45, 45, location.screenX, location.screenY));
                AbsoluteLayout.SetLayoutBounds(mainStack, new Rectangle(btnLocationX, btnLocationY, 45, 45));

                Menu.ItemTapped += (sender, e) =>
                {
                    var evnt = (SelectedItemChangedEventArgs)e;
                    string text = (string)evnt.SelectedItem;
                    if (text == "menucircle")
                    {
                        backlayout.IsVisible = true;
                        btnlayout.IsVisible = true;
                    }
                    else if (text == "closecircle")
                    {
                        btnlayout.IsVisible = false;
                        backlayout.IsVisible = false;
                    }
                };
                */
                DateTime date2 = DateTime.Now;
                string month = date2.ToString("MM");
                string Date = date2.ToString("dd");
                string year = date2.ToString("yyyy");
                string hour = date2.ToString("hh");
                string min = date2.ToString("mm");

                //DisplayAlert("detail", "Y: " + year + " M: " + month + " D: " + Date + " H: " + hour + " M: " + min, "ok");
                //string time2 = DateTime.Now.TimeOfDay.ToString();
                //DisplayAlert("time2", time2, "ok");
                //startTimepick.Time = DateTime.Now.TimeOfDay;

                
                if (App.selectedEvent == null)
                {
                    reminderPicker.SelectedIndex = 2;
                }

                foreach (string email in DependencyService.Get<CalendarConnect>().CalendarList())
                {
                    emailPicker.Items.Add(email);
                }

                // on save click
                try
                {
                    var tapGestureSave = new TapGestureRecognizer();
                    tapGestureSave.Tapped += (s, e) =>
                    {
                        //DisplayAlert("save", ".", ".");
                        SaveEventOnLabelClick();
                    };
                    lblSave.GestureRecognizers.Add(tapGestureSave);
                }
                catch (Exception x)
                {
                    System.Diagnostics.Debug.WriteLine("inner save" + x.Message);
                }
                // on cancel click
                try
                {
                    var tapGestureCancel = new TapGestureRecognizer();
                    tapGestureCancel.Tapped += (s, e) =>
                    {
                        //DisplayAlert("cancel", ".", ".");
                        CancelEventOnLabelClick();
                    };
                    lblCancel.GestureRecognizers.Add(tapGestureCancel);
                }
                catch (Exception x)
                {
                    System.Diagnostics.Debug.WriteLine("inner cancel" + x.Message);
                }

            }
            catch(Exception x) {
                DisplayAlert("Current Alert ", " " + x, "OK");

            }
        }

        public DateTime addHourInCurrentTime(DateTime date, int hour)
        {
            date = date.AddHours(hour);
            return date;
        }

        public string getSpecificDateFormat(DateTime date)
        {
            string dt = "";
            try
            {
                dt = date.ToString("ddd, dd MMM yyyy");
            }
            catch { }
            return dt;
        }

        public string getSpecificTimeFormat(TimeSpan time)
        {
            string dt = "";
            try
            {
                dt = time.ToString("HH:mm:ss");
            }
            catch { }
            return dt;
        }

        public string getSpecificTimeFormat2(DateTime time)
        {
            string dt = "";
            try
            {
                dt = time.ToString("HH:mm:ss");
            }
            catch { }
            return dt;
        }

        protected override bool OnBackButtonPressed()
        {
            Navigation.PushAsync(new CalendarView());
            base.OnBackButtonPressed();            
            return true;
        }

        private string monthConvert(string id)
        {
            string monthInEng = "";
            try
            {
                int month = Convert.ToInt16(id);
                if (month == 01)
                {
                    monthInEng = "JAN";
                }
                else if (month == 02)
                {
                    monthInEng = "FEB";
                }
                else if (month == 03)
                {
                    monthInEng = "MAR";
                }
                else if (month == 04)
                {
                    monthInEng = "APR";
                }
                else if (month == 05)
                {
                    monthInEng = "MAY";
                }
                else if (month == 06)
                {
                    monthInEng = "JUN";
                }
                else if (month == 07)
                {
                    monthInEng = "JULY";
                }
                else if (month == 08)
                {
                    monthInEng = "AUG";
                }
                else if (month == 09)
                {
                    monthInEng = "SEP";
                }
                else if (month == 10)
                {
                    monthInEng = "OCT";
                }
                else if (month == 11)
                {
                    monthInEng = "NOV";
                }
                else
                {
                    monthInEng = "DEC";
                }
            }
            catch { }
            return monthInEng;
        }

        public string[] ChangeDateFormat(DateTime date)
        {
            string dt = date.ToString();
            string[] dtArr = dt.Split(' ');
            return dtArr;

        }

        public string ChangeTimeFormat(DateTime time)
        {
            string dt = time.ToString();
            string[] dtArr = dt.Split(' ');
            return dtArr[0];
        }

        private async void SaveEventOnLabelClick()
        {
            try
            {
                string title = reminderTitle.Text;
                if (!string.IsNullOrWhiteSpace(title))
                {
                    //if(TimeSpan.Parse(startTimepick.Time.ToString()) != DateTime.Now.TimeOfDay)

                    DateTime startDate = startDatepick.Date;
                    DateTime endDate = Datepick.Date;

                    string SDATE = startDatepick.Date.ToString("dd-MM-yyyy");
                    string EDATE = Datepick.Date.ToString("dd-MM-yyyy");
                    string sTime = startTimepick.Time.ToString();
                    string eTime = EndTimepick.Time.ToString();

                    var defaultStartPickTime = Convert.ToDateTime(DateTime.Now.AddMinutes(11).ToString());
                    var startPickTime = Convert.ToDateTime(startDate.Add(startTimepick.Time)).ToLocalTime();
                    var defaultEndPickTime = Convert.ToDateTime(startDate.Add(startTimepick.Time).AddMinutes(30)).ToLocalTime();
                    var endPickTime = Convert.ToDateTime(endDate.Add(EndTimepick.Time)).ToLocalTime();


                    if (defaultStartPickTime < startPickTime || App.selectedEvent != null)
                    {
                        if (App.selectedEvent == null)
                        {
                            var diffTime = startPickTime - defaultStartPickTime;
                            var diffTimeHours = long.Parse(diffTime.Hours.ToString());
                            var diffTimeInt = long.Parse(diffTime.Minutes.ToString());
                            if (diffTimeHours > 0)
                            {
                                //await DisplayAlert("Warning", "Start time should be 15 minutes greater form current day of time", "OK");
                                //return;
                            }
                            else if ((diffTimeInt > 0 && diffTimeInt < 2) || (diffTimeInt < 1))
                            {
                                await DisplayAlert("Warning", "Start time should be 14 minutes greater form current day of time", "OK");
                                return;
                            }
                            else if ((diffTimeInt > 2 && diffTimeInt < 8))
                            {
                                if (reminderPicker.SelectedIndex == 2 || reminderPicker.SelectedIndex == 1)
                                {
                                    await DisplayAlert("Warning", "you can't select 20 mins before, Notification or 30 mins before, Notification, Because start time should be 23 or 33 minutes greater form current day of time", "OK");
                                    return;
                                }
                            }
                            else if ((diffTimeInt > 8 && diffTimeInt < 18))
                            {
                                if (reminderPicker.SelectedIndex == 2)
                                {
                                    await DisplayAlert("Warning", "you can't 30 mins before, Notification, Because start time should be 33 minutes greater form current day of time", "OK");
                                    return;
                                }
                            }
                        }
                        if (defaultEndPickTime < endPickTime)
                        {
                            //await DisplayAlert("DATE", "SDATE: " + SDATE + " EDATE: " + EDATE, "ok");                    
                            //await DisplayAlert("TIME", "sTime: " + sTime + " eTime: " + eTime, "ok");

                            string shour = startTimepick.Time.ToString("hh");
                            string smin = startTimepick.Time.ToString("mm");

                            string ehour = EndTimepick.Time.ToString("hh");
                            string emin = EndTimepick.Time.ToString("mm");

                            string smonth = startDate.ToString("MM");
                            smonth = monthConvert(smonth);
                            string sDate = startDate.ToString("dd");
                            string syear = startDate.ToString("yyyy");
                            string emonth = endDate.ToString("MM");
                            string eDate = endDate.ToString("dd");
                            string eyear = endDate.ToString("yyyy");

                            string overallTime = sTime + " - " + eTime;
                            //  await DisplayAlert(title, "sTime: " + sTime + " smin: " + smin + " syear: " + syear + " smonth: " + smonth + " sDate: " + sDate + " eTime: " + eTime + " emin: " + emin + " eyear: " + eyear + " emonth: " + emonth + " eDate: " + eDate, "OK");


                            string EmailStatus = ""; string status = ""; string reminderIndex = "";
                            string EmailIndex = "";
                            // email
                            try
                            {
                                EmailIndex = emailPicker.Items[emailPicker.SelectedIndex]; // emailPicker.SelectedIndex;
                                if (EmailIndex != "")
                                {
                                    // reminder
                                    try
                                    {
                                        

                                        reminderIndex = reminderPicker.Items[reminderPicker.SelectedIndex]; // reminderPicker.SelectedIndex;
                                        string respone = DependencyService.Get<CalendarConnect>().AddEvent(title, startDate, shour, smin, endDate, ehour, emin);
                                        int id = -1;
                                        string evttitle = "";
                                        string dateString = "";
                                        Task.Run(async () =>
                                        {
                                            id = await SaveEvents(title, SDATE, sDate, smonth, sTime, EDATE, eTime, overallTime, EmailIndex, reminderIndex);
                                            if (App.selectedEvent != null)
                                            {
                                                CrossLocalNotifications.Current.Cancel(App.selectedEvent.ID);
                                                id = App.selectedEvent.ID;
                                                evttitle = App.selectedEvent.title;
                                                dateString = "Start Date : " + App.selectedEvent.startDate + " & End Date" + App.selectedEvent.endDate;
                                            }
                                            else {
                                                var eve = await App.Database.GetItemEventsLast();
                                                id  = eve.ID;
                                                evttitle = eve.title;
                                                dateString = "Start Date : " + eve.startDate + " & End Date" + eve.endDate;
                                            }
                                        }).Wait();
                                        
                                        var date = startPickTime;
                                        if (switchtoggle.IsToggled)
                                        {
                                            // long one_day = 1000 * 60 * 60 * 24;                                                                                    
                                            var difference_ms = Datepick.Date - startDatepick.Date;
                                            // var rounded = Convert.ToDouble(long.Parse(difference_ms.TotalDays.ToString())) / one_day;
                                            // var il = Math.Round(rounded);
                                            var ill = difference_ms.Days + 1;
                                            for (var i = 0; i < ill; i++)
                                            {
                                                if (reminderPicker.SelectedIndex == 2)
                                                {
                                                    date = date.AddDays(i).Add(new TimeSpan(0, -30, 0));
                                                }
                                                else if (reminderPicker.SelectedIndex == 1)
                                                {
                                                    date = date.AddDays(i).Add(new TimeSpan(0, -20, 0));
                                                }
                                                else if (reminderPicker.SelectedIndex == 0)
                                                {
                                                    date = date.AddDays(i).Add(new TimeSpan(0, -10, 0));
                                                }
                                                else
                                                {
                                                    date = date.Add(new TimeSpan(0, -30, 0));
                                                }
                                                int newID = int.Parse(i + "000" + id.ToString());
                                                CrossLocalNotifications.Current.Show(evttitle, dateString, newID, date);

                                            }
                                        }
                                        else
                                        {
                                            if (reminderPicker.SelectedIndex == 2)
                                            {
                                                date = date.Add(new TimeSpan(0, -30, 0));
                                            }
                                            else if (reminderPicker.SelectedIndex == 1)
                                            {
                                                date = date.Add(new TimeSpan(0, -20, 0));
                                            }
                                            else if (reminderPicker.SelectedIndex == 0)
                                            {
                                                date = date.Add(new TimeSpan(0, -10, 0));
                                            }
                                            else
                                            {
                                                date = date.Add(new TimeSpan(0, -30, 0));
                                            }
                                            CrossLocalNotifications.Current.Show(evttitle, dateString, id, date);

                                        }
                                        await DisplayAlert("CloudSchool", "Event successfully created", "ok");
                                        App.selectedEvent = null;
                                        App.PopMasterDetail(1);
                                    }
                                    catch (Exception x)
                                    {
                                        await DisplayAlert("inner", x.Message, "ok");
                                    }
                                }
                                else
                                {
                                    await DisplayAlert("EmailIndex", "Please select Email", "ok");

                                }
                            }
                            catch
                            {
                                await DisplayAlert("EmailIndex", "Please select Email", "ok");
                            }
                        }
                        else
                        {
                            await DisplayAlert("Warning", "End time should be 29 minutes greater form current day of time", "OK");
                            return;
                        }
                    }
                    else
                    {
                        var diffTime = startPickTime - defaultStartPickTime;
                        var diffTimeInt = long.Parse(diffTime.Minutes.ToString());
                        if ((diffTimeInt > 0 && diffTimeInt < 2) || (diffTimeInt < 1))
                        {
                            await DisplayAlert("Warning", "Start time should be 14 minutes greater form current day of time", "OK");
                            return;
                        }
                    }

                    // DisplayAlert("Status", "EmailPick: " + EmailIndex + " ReminderStatus: " + status + " Date: " + date.ToString() + " time: " + time.ToString(),  "OK");
                }
                else
                {
                    await DisplayAlert("Title", "Title can not be Empty", "OK");
                    reminderTitle.Focus();
                }
            }
            catch (Exception ex)
            {
                await DisplayAlert("error in Create Event", "Error: " + ex.Message, "ok");
            }
        }

        public async Task<int> SaveEvents(string remTitle, string sDatepick, string sDate, string smonth, string sTimepick, string eDatepick, string eTimepick, string overallTime, string Email, string reminderTime)
        {
            try
            {
                if (App.selectedEvent != null)
                {
                    App.selectedEvent.title = remTitle;
                    App.selectedEvent.startDate = sDatepick;
                    App.selectedEvent.strtDate = sDate;
                    App.selectedEvent.strtMonth = smonth;
                    App.selectedEvent.startTime = sTimepick;
                    App.selectedEvent.endDate = eDatepick;
                    App.selectedEvent.endTime = eTimepick;
                    App.selectedEvent.overallTime = overallTime;
                    App.selectedEvent.email = Email;
                    App.selectedEvent.notificationTime = reminderTime;
                    int result = await App.Database.SaveEvents(App.selectedEvent);                    
                    //App.selectedEvent = null;
                    //App.PopMasterDetail(1);
                    return result;
                }
                else
                {
                    EventsList OReg = new EventsList();
                    OReg.title = remTitle;
                    OReg.startDate = sDatepick;
                    OReg.strtDate = sDate;
                    OReg.strtMonth = smonth;
                    OReg.startTime = sTimepick;
                    OReg.endDate = eDatepick;
                    OReg.endTime = eTimepick;
                    OReg.overallTime = overallTime;
                    OReg.email = Email;
                    OReg.notificationTime = reminderTime;
                    int result = await App.Database.SaveEvents(OReg);
                    //App.PopMasterDetail(1);
                    return result;
                    //await DisplayAlert("res", result.ToString(), "ok");  
                }             
            }
            catch (Exception e)
            {
                await DisplayAlert("error", e.Message, "ok");
                return -1;
            }

        }

        private void CancelEventOnLabelClick()
        {
            try
            {
                App.NavigateMasterDetail(new CalendarView());
            }
            catch { }
        }

        private void reminderPicker_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void emailPicker_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private async void event_lists_Clicked(object sender, EventArgs e)
        {
            try
            {
                await Navigation.PushAsync(new Events());
            }
            catch { }
        }

        private void Datepick_DateSelected(object sender, DateChangedEventArgs e)
        {

            lblDatePicked.Text = getSpecificDateFormat(Datepick.Date);
        }

        private void startDatepick_DateSelected(object sender, DateChangedEventArgs e)
        {
            lblSDatePicked.Text = getSpecificDateFormat(startDatepick.Date);
        }
        
        private void startTimepick_PropertyChanged_1(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == TimePicker.TimeProperty.PropertyName)
            {
                 lblStartTimePicked.Text = startTimepick.Time.ToString();
            }
        }

        private void EndTimepick_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == TimePicker.TimeProperty.PropertyName)
            {
                lblEndTimePicked.Text = EndTimepick.Time.ToString();
            }
        }
    }
}