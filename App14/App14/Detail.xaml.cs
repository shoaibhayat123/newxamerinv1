﻿using App14.Models;
using Newtonsoft.Json;
using Plugin.Connectivity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App14
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Detail : ContentPage
    {
        location loc = new location();
        public static double btnLocationX;
        public static double btnLocationY;
        ComClass comfun = new ComClass();
        public int reminders = 0;

        public Detail()
        {
            try
            {
                InitializeComponent();
               
               // DisplayAlert("Network Connection", comfun.isConnected().ToString(), "ok");
                AbsoluteLayout.SetLayoutBounds(absMain, new Rectangle(0, 0, location.screenX, location.screenY - 25));
                /*
                btnLocationX = location.btnMenuLocationX;
                btnLocationY = location.btnMenuLocationY;
                AbsoluteLayout.SetLayoutBounds(absMain, new Rectangle(0, 0, location.screenX, location.screenY - 25));
                AbsoluteLayout.SetLayoutBounds(backlayout, new Rectangle(0, 0, location.screenX, location.screenY));
                AbsoluteLayout.SetLayoutBounds(btnlayout, new Rectangle(45, 45, location.screenX , location.screenY));
                AbsoluteLayout.SetLayoutBounds(mainStack, new Rectangle(btnLocationX, btnLocationY, 45, 45));
                
                Menu.ItemTapped += (sender, e) =>
                {
                    var evnt = (SelectedItemChangedEventArgs)e;
                    string text = (string)evnt.SelectedItem;
                    if(text == "menucircle")
                    {
                      //  App.NavigateMasterDetail(new DeviceInfo());
                        backlayout.IsVisible = true;
                        btnlayout.IsVisible = true;
                    }
                    else if(text == "closecircle")
                    {
                        btnlayout.IsVisible = false;
                        backlayout.IsVisible = false;
                    }
                };
                */

                getDashBoardData();
                
                // on create itemWarnings
                try
                {
                    var tapGestureitemWarnings = new TapGestureRecognizer();
                    tapGestureitemWarnings.Tapped += (s, e) =>
                    {
                        try
                        {
                            App.NavigateMasterDetail(new Warnings());
                        }
                        catch { }
                    };
                    itemWarnings.GestureRecognizers.Add(tapGestureitemWarnings);
                }
                catch { }
                
                // on create itemOnlineDevices
                try
                {
                    var tapGestureitemOnlineDevices = new TapGestureRecognizer();
                    tapGestureitemOnlineDevices.Tapped += (s, e) =>
                    {
                        try
                        {
                            App.NavigateMasterDetail(new onlineDevices());
                        }
                        catch { }
                    };
                    itemOnlineDevices.GestureRecognizers.Add(tapGestureitemOnlineDevices);
                }
                catch { }

                // on create itemConnectedDevices
                try
                {
                    var tapGestureitemConnectedDevices = new TapGestureRecognizer();
                    tapGestureitemConnectedDevices.Tapped += (s, e) =>
                    {
                        try
                        {
                            App.NavigateMasterDetail(new Rooms());
                        }
                        catch { }
                    };
                    itemConnectedDevices.GestureRecognizers.Add(tapGestureitemConnectedDevices);
                }
                catch { }

                // on create itemSecFrstStak
                try
                {
                    var tapGestureitemSecFrstStak = new TapGestureRecognizer();
                    tapGestureitemSecFrstStak.Tapped += (s, e) =>
                    {
                        try
                        {
                            App.NavigateMasterDetail(new Tickets());
                        }
                        catch { }
                    };
                    SecFrstStak.GestureRecognizers.Add(tapGestureitemSecFrstStak);
                }
                catch { }

                // on calender
                try
                {
                    var tapGesturelblTodolist = new TapGestureRecognizer();
                    tapGesturelblTodolist.Tapped += (s, e) =>
                    {
                        try
                        {
                            App.NavigateMasterDetail(new CalendarView());
                        }
                        catch { }
                    };
                    TODOLIST.GestureRecognizers.Add(tapGesturelblTodolist);
                }
                catch { }

                // on chat
                try
                {
                    var tapGesturelblCHAT = new TapGestureRecognizer();
                    tapGesturelblCHAT.Tapped += (s, e) =>
                    {
                        try
                        {
                            //App.NavigateMasterDetail(new Chat());
                            //DisplayAlert("session dashboard result", App.session_string, "OK");
                            App.NavigateMasterDetail(new InAppBrowser("https://creative9.cloudschool.management/admin/chat/?accessid=" + App.session_string, "Chat"));

                        }
                        catch { }
                    };
                    CHAT.GestureRecognizers.Add(tapGesturelblCHAT);
                }
                catch { }
            }
            catch { }

        }

        protected override async void OnAppearing()
        {
            base.OnAppearing();
            Task.Run(async () =>
            {
                var list = await App.Database.getAllEvents();
                List<EventsList> events = new List<EventsList>();
                foreach (var lst in list)
                {
                    var i = DateTime.ParseExact(lst.endDate, "d-M-yyyy", new System.Globalization.CultureInfo("fi-FI"));
                    var endtime = i.Add(TimeSpan.Parse(lst.endTime));
                    var starttime = DateTime.Now.ToLocalTime();
                    var diffTime = endtime - starttime;
                    //var diffTimeInt = long.Parse(diffTime.TotalMinutes.ToString());

                    if (diffTime.TotalMinutes < 1)
                    {
                        await App.Database.deleteEvent(lst);
                    }

                }
            }).Wait();

            reminders = await App.Database.noOfEvents();
            lblReminders.Text = reminders.ToString();
        }


        private async void getDashBoardData()
        {
            // getting timesheet id & timesheet status
            if (comfun.isConnected())
            {
                try
                {
                    if (App.session_string != "")
                    {
                        var client = new HttpClient();
                        client.BaseAddress = new Uri(App.api_url);
                        var values = new Dictionary<string, string>();
                        values.Add("session_string", App.session_string);
                        var content = new FormUrlEncodedContent(values);
                        HttpResponseMessage response = await client.PostAsync("/itcrm/getDashboardLogs/", content);
                        var result = await response.Content.ReadAsStringAsync();
                        //await DisplayAlert("getting dashboard result", result.ToString(), "OK");
                        statusCheck chk_status = JsonConvert.DeserializeObject<statusCheck>(result);
                        //await DisplayAlert("status!", chk_status.status.ToString(), "ok");
                        if (chk_status.status)
                        {
                            // await DisplayAlert("status!", "true", "ok");
                            Dashboard Dashboard_list = JsonConvert.DeserializeObject<Dashboard>(result);
                            var detail = Dashboard_list.result[0];
                            try
                            {
                                lblNoOfConDevices.Text = detail.devices;
                                lblOnlineDevices.Text = detail.online;
                                lblWarnings.Text = detail.warnings;
                                lblTickets.Text = detail.tickets;
                                //lblTodolist.Text = detail.todo;
                                //lblCSFixIt.Text = detail.LCSFI;
                                lblReminders.Text = detail.todo;
                                aiDevices.IsRunning = false;

                                //await DisplayAlert("App.ticketID!", App.ticketID, "OK");
                                if (App.ticketID != "")
                                {
                                    App.NavigateMasterDetail(new EditTicket(App.ticketID));
                                }
                            }
                            catch (Exception e)
                            {
                                //await DisplayAlert("Error!", e.Message, "OK");
                            }
                            aiDevices.IsRunning = false;
                        }

                        Task.Run(async () =>
                        {
                            var list = await App.Database.getAllEvents();
                            List<EventsList> events = new List<EventsList>();
                            foreach (var lst in list)
                            {
                                var i = DateTime.ParseExact(lst.endDate, "d-M-yyyy", new System.Globalization.CultureInfo("fi-FI"));
                                var endtime = i.Add(TimeSpan.Parse(lst.endTime));
                                var starttime = DateTime.Now.ToLocalTime();
                                var diffTime = endtime - starttime;
                                //var diffTimeInt = long.Parse(diffTime.TotalMinutes.ToString());
                             
                                if (diffTime.TotalMinutes < 1)
                                {
                                    await App.Database.deleteEvent(lst);
                                }

                            }
                        }).Wait();

                        reminders = await App.Database.noOfEvents();
                        lblReminders.Text = reminders.ToString();
                    }
                    else
                    {
                        Task.Run(async () =>
                        {
                            await App.deleteAllRememberCredentials();
                            await App.deleteAllRegisCredentials();
                        }).Wait();
                        await Navigation.PushAsync(new SchoolLog());
                    }
                }
                catch (Exception e)
                {
                    // await DisplayAlert("Error!", e.Message, "OK");
                }

              // await  DisplayAlert("no", App.Database.noOfEvents() + " yes", "ok");
            }
            else
            {
                await DisplayAlert("Connection", "Internet Connection Disabled", "Ok");
            }
        }

        protected override bool OnBackButtonPressed()
        {
            base.OnBackButtonPressed();
            return true;
        }

        private void btnWarning_Clicked(object sender, EventArgs e)
        {
            App.NavigateMasterDetail(new Warnings());
        }

        private void btnTicketsDetail_Clicked(object sender, EventArgs e)
        {
            App.NavigateMasterDetail(new Tickets());
        }

        private void btnRooms_Clicked(object sender, EventArgs e)
        {
            App.NavigateMasterDetail(new Rooms());
        }

        private void btnHelp_Clicked(object sender, EventArgs e)
        {
            try
            {
                Device.OpenUri(new Uri("http://cloudschool.management/itcrm/helpPortal"));
            }
            catch { }
        }

        
    }
    public class SetDashBoardList
    {
        public string online { get; set; }
        public string devices { get; set; }
        public string tickets { get; set; }
        public string warnings { get; set; }
        public string todo { get; set; }
        public string LCSFI { get; set; }
    }
}