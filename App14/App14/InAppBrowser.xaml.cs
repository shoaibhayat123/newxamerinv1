﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App14
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InAppBrowser : ContentPage
    {
        /// <summary>
		/// Initializes a new instance of the <see cref="WebViewSample.InAppBrowserXaml"/> class.
		/// Takes a URL indicating the starting page for the browser control.
		/// </summary>
		/// <param name="URL">URL to display in the browser.</param>
		public InAppBrowser(string URL, string title)
        {
            InitializeComponent();
            Title = title;
            webView.Source = URL;
        }

        /// <summary>
        /// fired when the back button is clicked. If the browser can go back, navigate back.
        /// If the browser can't go back, leave the in-app browser page.
        /// </summary>
        void backButtonClicked(object sender, EventArgs e)
        {
            if (webView.CanGoBack)
            {
                webView.GoBack();
            }
            else
            {
                this.Navigation.PopAsync(); // closes the in-app browser view.
            }
        }

        /// <summary>
        /// Navigates forward
        /// </summary>
        void forwardButtonClicked(object sender, EventArgs e)
        {
            if (webView.CanGoForward)
            {
                webView.GoForward();
            }
        }
    }
}