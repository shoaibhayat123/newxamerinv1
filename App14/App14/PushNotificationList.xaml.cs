﻿using App14.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace App14
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class PushNotificationList : ContentPage
    {
        public PushNotificationList()
        {
            InitializeComponent();
        }

        private async void lvEventsList_ItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                var pushnotify = e.SelectedItem as PushNotificationData;
                App.NavigateMasterDetail(new TicketDetails(pushnotify.recordid));
                //  App.PopMasterDetail(1);
            }
        }
        public int data = 0;
        protected async override void OnAppearing()
        {
            base.OnAppearing();

            try
            {
                List<PushNotificationData> pushNotifylst = await App.Database.GetAllPushNotification();
                var lst = pushNotifylst.GroupBy(unit => new { unit.recordid, unit.created, unit.user_id, unit.body, unit.title, unit.message, unit.content_available }).Select(x => x.First()).ToList();
                lvpushList.ItemsSource = lst;
            }
            catch (Exception x) {  }
        }

        protected override bool OnBackButtonPressed()
        {
            base.OnBackButtonPressed();
            return true;
        }
    }

}