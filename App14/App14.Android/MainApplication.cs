﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Android.App;
using Android.Content;
using Android.OS;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Plugin.FirebasePushNotification;
using System.Threading.Tasks;
using Firebase.Iid;
using System.Net.Http;
using Android.Util;
using App14.Models;

namespace App14.Droid
{
    [Application]
    public class MainApplication : Application
    {
        public MainApplication(IntPtr handle, JniHandleOwnership transer) : base(handle, transer)
        {
        }

        public override void OnCreate()
        {
            base.OnCreate();

            //If debug you should reset the token each time.
#if DEBUG
            FirebasePushNotificationManager.Initialize(this, true);
#else
              FirebasePushNotificationManager.Initialize(this,false);
#endif

            CrossFirebasePushNotification.Current.OnTokenRefresh += async (s, p) =>
            {
                string TAG = "Cloudschool";
                Log.Info(TAG, $"This is a DEVICE TOKEN : {p.Token}");
                Log.Info(TAG, $"This is a App.session_string : {App.session_string}");
                App.device_token = p.Token;

                int count = await App.getAllRegisCredentials();
                if (count > 1)
                {                

                //RegEntity userDetail = await App.Database.GetItemFirst();  // commit by shoaib
                //if (userDetail != null)
                //{
                //    App.school_name = userDetail.school_name;
                //    App.api_url = userDetail.CompleteUrl;

                    RememberMeCredentials userCredentials = await App.Database.GetRemeber();
                    if (userCredentials != null)
                    {
                        App.user_id = userCredentials.user_id;
                        App.tenant_id = userCredentials.user_tenant_id;
                        var os = "android";
                        Log.Info(TAG, $"This is a IS Platform  : {os}");
                        var notify = new Dictionary<string, string>();
                        notify.Add("operation", "insert_device_token");
                        notify.Add("user_id", App.user_id);
                        notify.Add("parent_id", App.tenant_id);
                        notify.Add("device_token", p.Token);
                        notify.Add("device_type", os);
                        var client = new HttpClient();
                        Log.Info(TAG, $"This is a IS App.api_url  : {App.api_url}");
                        client.BaseAddress = new Uri(App.api_url);
                        var notify_content = new FormUrlEncodedContent(notify);
                        HttpResponseMessage notify_response = await client.PostAsync("/itcrm/webservices/", notify_content);
                        var notify_result = await notify_response.Content.ReadAsStringAsync();
                        Log.Info(TAG, $"This is a Result : {notify_result}");
                    }
                }
            };


            //Handle notification when app is closed here
            CrossFirebasePushNotification.Current.OnNotificationReceived += async (s, p) =>
            {
                string TAG = "Cloudschool";
                PushNotificationData saveNotifyInfo = new PushNotificationData();
                foreach (var data in p.Data)
                {
                    Log.Info(TAG, $"This Data {data.Key} : {data.Value}");
                    System.Diagnostics.Debug.WriteLine($"{data.Key} : {data.Value}");
                    if (data.Key == "recordid")
                    {
                        App.ticketID = data.Value.ToString();
                        saveNotifyInfo.recordid = data.Value.ToString();
                        Log.Debug(TAG, $"This is a debug message : {App.device_token}");
                    }
                    if (data.Key == "created")
                    {
                        saveNotifyInfo.created = data.Value.ToString();
                    }
                    if (data.Key == "user_id")
                    {
                        saveNotifyInfo.user_id = data.Value.ToString();
                    }
                    if (data.Key == "body")
                    {
                        saveNotifyInfo.body = data.Value.ToString();
                    }
                    if (data.Key == "title")
                    {
                        saveNotifyInfo.title = data.Value.ToString();
                    }
                    if (data.Key == "click_action")
                    {
                        saveNotifyInfo.click_action = data.Value.ToString();
                    }
                    if (data.Key == "message")
                    {
                        saveNotifyInfo.message = data.Value.ToString();
                    }
                    if (data.Key == "content_available")
                    {
                        saveNotifyInfo.content_available = data.Value.ToString();
                    }
                    if (data.Key == "sound")
                    {
                        saveNotifyInfo.sound = data.Value.ToString();
                    }
                    if (data.Key == "screen")
                    {
                        saveNotifyInfo.screen = data.Value.ToString();
                    }

                }
                var rec = await App.Database.SavePushNotification(saveNotifyInfo);
                Log.Info(TAG, $"This Notification Data ID : {rec}");
            };

        }
    }

}