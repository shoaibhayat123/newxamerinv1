﻿using System;

using Android.App;
using Android.Content.PM;
using Android.Runtime;
using Android.Views;
using Android.Widget;
using Android.OS;
using Android.Net;
using Android.Content.Res;
using Plugin.FirebasePushNotification;
using Plugin.FirebasePushNotification.Abstractions;
using System.Collections.Generic;
using Android.Util;
using Android.Accounts;

namespace App14.Droid
{
    //, ScreenOrientation = ScreenOrientation.Portrait
    [Activity(LaunchMode = LaunchMode.SingleTop, Label = "App14", Icon = "@drawable/icon", Theme = "@style/MainTheme", MainLauncher = false, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {

        protected override void OnCreate(Bundle bundle)
        {
            RequestedOrientation = ScreenOrientation.Portrait;
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(bundle);
            //allowing the device to change the screen orientation based on the rotation
            Xamarin.Forms.MessagingCenter.Subscribe<WebView>(this, "allowLandScapePortrait", sender => { RequestedOrientation = ScreenOrientation.Landscape; });


            global::Xamarin.Forms.Forms.Init(this, bundle);

            LoadApplication(new App());
            FirebasePushNotificationManager.ProcessIntent(Intent, false);
            var emailPattern = Patterns.EmailAddress; // API level 8+
            Android.Content.Context context = Android.App.Application.Context;
            Account[] accounts = AccountManager.Get(context).GetAccounts();
            foreach (Account account in accounts)
            {
                if (emailPattern.Matcher(account.Name).Matches())
                {
                    String possibleEmail = account.Name;
                    App.accountEmail = possibleEmail;
                    System.Diagnostics.Debug.WriteLine($"Account Email : {App.accountEmail}");
                }
            }
            //            FirebasePushNotificationManager.ProcessIntent(Intent);
            //#if DEBUG
            //            FirebasePushNotificationManager.Initialize(this,
            //                          new NotificationUserCategory[]
            //                          {
            //                    new NotificationUserCategory("message",new List<NotificationUserAction> {
            //                        new NotificationUserAction("Reply","Reply", NotificationActionType.Foreground),
            //                        new NotificationUserAction("Forward","Forward", NotificationActionType.Foreground)

            //                    }),
            //                    new NotificationUserCategory("request",new List<NotificationUserAction> {
            //                    new NotificationUserAction("Accept","Accept", NotificationActionType.Default, "check"),
            //                    new NotificationUserAction("Reject","Reject", NotificationActionType.Default, "cancel")
            //                    })
            //                          }, true);
            //#else
            //  FirebasePushNotificationManager.Initialize(this,
            //                new NotificationUserCategory[]
            //                {
            //                    new NotificationUserCategory("message",new List<NotificationUserAction> {
            //                        new NotificationUserAction("Reply","Reply", NotificationActionType.Foreground),
            //                        new NotificationUserAction("Forward","Forward", NotificationActionType.Foreground)

            //                    }),
            //                    new NotificationUserCategory("request",new List<NotificationUserAction> {
            //                    new NotificationUserAction("Accept","Accept", NotificationActionType.Default, "check"),
            //                    new NotificationUserAction("Reject","Reject", NotificationActionType.Default, "cancel")
            //                    })
            //                }, false);
            //#endif    
                   
        }


    }
}

